﻿using CandiceAIforGames.AI;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandiceDamageReceiver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    { 

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CandiceReceiveDamage(float damage)
    {
       if(gameObject.tag == "Player")
        {
            HeroKnight player = GetComponentInParent<HeroKnight>();
            if (player.isBlocking) damage *= .88f;

            player.Health -= damage;
            Debug.Log("Received " + damage.ToString() + " damage");
        }
        else if(gameObject.tag == "Enemy" || gameObject.tag == "Candice Agent" || gameObject.tag == "Angel")
        {
            gameObject.GetComponent<CandiceAIController>().HitPoints -= GetComponentInParent<HeroKnight>().m_attackDamage;
        }

    }
}
