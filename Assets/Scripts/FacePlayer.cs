using System.Collections;
using System.Collections.Generic;

using Unity.VisualScripting;

using UnityEngine;

public class FacePlayer : MonoBehaviour
{
    public HeroKnight Player;
    public float BounceBack;
    private Vector2 default_vel;

    // Start is called before the first frame update
    void Start()
    {
        if (Player == null) Debug.LogError("No Player assigned to FacePlayer script");
        var velocity = gameObject.GetComponent<Rigidbody2D>();
        this.default_vel = velocity.velocity;
    }

    // Update is called once per frame
    void Update()
    {

        if (gameObject.tag == "AngelEnemy")
        {
            Rigidbody2D angel = gameObject.GetComponent<Rigidbody2D>();


            if (gameObject.transform.position.y <= Player.transform.position.y + Player.transform.localScale.y)
            {   
                angel.velocity.Set(angel.velocity.x, -angel.velocity.y * BounceBack);
            }
            else
            {
                angel.velocity.Set(angel.velocity.x, angel.velocity.y);
            }
        }

        if (Player.transform.position.x > gameObject.transform.position.x)
            gameObject.GetComponent<SpriteRenderer>().flipX = true;

        else gameObject.GetComponent<SpriteRenderer>().flipX = false;
    }
}
