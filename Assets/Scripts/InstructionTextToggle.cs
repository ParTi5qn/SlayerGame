using System.Collections;
using System.Collections.Generic;

using TMPro;

using UnityEditor.AssetImporters;

using UnityEngine;
using UnityEngine.UI;

public class InstructionTextToggle : MonoBehaviour
{
    public Text text;

    public bool toggled = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            this.text.gameObject.SetActive(toggled);
            toggled = !toggled;
        }
    }
}
