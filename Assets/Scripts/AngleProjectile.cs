using System.Collections;
using System.Collections.Generic;

using Unity.VisualScripting.FullSerializer;

using UnityEngine;

public class AngleProjectile : MonoBehaviour
{
    public float RotationSpeed = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Rotate(new Vector3(0,0,1), -RotationSpeed);
    }

    private void FixedUpdate()
    {
    }
}
