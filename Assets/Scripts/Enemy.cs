using CandiceAIforGames.AI;

using JetBrains.Annotations;

using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Animator animator;
    public HeroKnight player;
    public float attackRange;
    public int Health = 80;
    public float knockbackForce;

    private bool dead => this.Health <= 0;

    // Start is called before the first frame update
    void Start()
    {
        this.animator = GetComponent<Animator>();
        this.player = FindObjectOfType<HeroKnight>();
        this.attackRange = GetComponent<CandiceAIController>().AttackRange;
    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector3.Distance(player.transform.position, gameObject.transform.position);
        if (player.isAttacking && distanceToPlayer < attackRange * 5 && !dead)
        {
            this.Health -= 15;

            var diff = gameObject.transform.position - player.transform.position;
            diff = diff.normalized * knockbackForce;
            gameObject.GetComponent<Rigidbody2D>().AddForce(diff, ForceMode2D.Impulse);
        }

        if (this.dead)
        {
            animator.SetBool("Idle", false);
            this.animator.SetTrigger("Death");
            return;
        }
    }

    public void EnemyDied(string ok)
    {
        gameObject.SetActive(false);
    }

}
